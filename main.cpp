#include <iostream>
#include <oskar/telescope/station/oskar_evaluate_tec_screen.h>


int main() {

    int num_points;
    oskar_Mem *l;
    oskar_Mem *m;
    double station_u_m;
    double station_v_m;
    double frequency_hz;
    double screen_height_m;
    double screen_pixel_size_m;
    int screen_num_pixels_x;
    int screen_num_pixels_y;
    int screen_num_pixels_t;
    oskar_Mem* tec_screen;
    int offset_out;
    oskar_Mem* out;
    int status;

    char tec_screen_path[] = "screen.fits";

    int num_axes = 0;
    int* axis_size = 0;
    oskar_mem_read_fits(0, 0, 0,
            tec_screen_path, 0, 0,
            &num_axes, &axis_size, 0, &status);

    std::cout << status << std::endl;
    exit(0);
    
    screen_num_pixels_x = axis_size[0];
    screen_num_pixels_y = axis_size[1];
    screen_num_pixels_t = axis_size[2];
    free(axis_size);

    exit(0);


    oskar_evaluate_tec_screen(
        num_points,
        l,
        m,
        station_u_m,
        station_v_m,
        frequency_hz,
        screen_height_m,
        screen_pixel_size_m,
        screen_num_pixels_x,
        screen_num_pixels_y,
        tec_screen,
        offset_out,
        out,
        &status);
 
}

